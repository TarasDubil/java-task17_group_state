package com.group.view;

import com.group.controller.ControllerImpl;
import com.group.model.Task;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewImpl implements View {
    private static final String blue = "\u001B[34m";
    private static final String purple = "\u001B[35m";
    private static final String red = "\u001B[31m";

    private ControllerImpl controller;
    private Map<String, String> map;
    private Map<String, String> userMap;
    private Scanner scanner = new Scanner(System.in);

    public ViewImpl() {
        controller = new ControllerImpl();
        map = new LinkedHashMap<>();
        map.put("1", "1. Get all users.");
        map.put("2", "2. Get all tasks.");
        map.put("3", "3. Create user.");
        map.put("4", "4. Create task.");
        map.put("5", "5. Choice user.");
        map.put("Q", "Q. Quit.");
        userMap = new LinkedHashMap<>();
        userMap.put("1", "1. Set user task.");
        userMap.put("2", "2. Change status task.");
        userMap.put("Q", "Q. Quit.");
    }

    public void outputMenu() {
        System.out.println(red + "\nMENU:");
        for (String str : map.values()) {
            System.out.println(blue + str);
        }
        System.out.println(purple + "\nMake your choice");
        String userChoice = scanner.nextLine().toUpperCase();
        switch (userChoice) {
            case "1":
                if (controller.getUsers().size() != 0) {
                    System.out.println(controller.getUsers());
                    outputMenu();
                } else {
                    System.out.println(red + "List 'users' is empty");
                    outputMenu();
                }
                break;
            case "2":
                    controller.getAllTasks();
                    outputMenu();
                break;
            case "3":
                System.out.println("Enter user name: ");
                String name = scanner.nextLine();
                controller.createUser(name);
                System.out.println(controller.getUsers());
                outputMenu();
                break;
            case "4":
                System.out.println("Enter task description: ");
                String taskDescription = scanner.nextLine();
                controller.createTask(taskDescription);
                controller.getAllTasks();
                outputMenu();
                break;
            case "5":
                if (controller.getUsers().size() != 0) {
                    chooseUserMenu();
                } else {
                    System.out.println("List 'users' is empty");
                    outputMenu();
                }
            case "Q":
                System.exit(0);
            default:
                System.out.println("Bad choice");
                outputMenu();
        }
    }

    private void chooseUserMenu() {
        System.out.println(controller.getUsers());
        System.out.println("Make your choice");
        String userChoice = scanner.nextLine().toUpperCase();
        try {
            controller.setUser(controller.getUsers().get(Integer.parseInt(userChoice) - 1));
        } catch (Exception e) {
            System.out.println("try again");
            chooseUserMenu();
        }

        System.out.println(controller.getUser());
        userMenu();
    }

    private void userMenu() {
        System.out.println(red + "\nMENU:");
        for (String str : userMap.values()) {
            System.out.println(blue + str);
        }
        System.out.println(purple + "\nMake your choice");
        String userChoice = scanner.nextLine().toUpperCase();
        switch (userChoice) {
            case "1":
                if (controller.getTasks().size() != 0) {
                    try {
                        controller.getAllTasks();
                        System.out.println(purple + "choose task:");
                        String s = scanner.nextLine().toUpperCase();
                        Task task = controller.getTasks().get(Integer.parseInt(s) - 1);
                        controller.giveUserTask(task);
                        System.out.println(controller.getUser());
                        userMenu();
                    } catch (Exception e) {
                        System.out.println("This task doesnt exist. Try again.");
                        userMenu();
                    }

                } else {
                    System.out.println("List 'tasks' is empty");
                    userMenu();
                }
                break;
            case "2":
                controller.changeTaskStatus();
                userMenu();
                break;
            case "Q":
                outputMenu();
            default:
                System.out.println("Bad choice");
                userMenu();
        }
    }
}
