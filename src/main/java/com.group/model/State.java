package com.group.model;

public interface State {
    Kanban kanban = Kanban.getInstance();

    default void newTask(String taskDescription) {
        System.out.println("cant add new task");
    }

    default void inProgress(Task task) {
        System.out.println("inProgress is not allowed");
    }

    default void codeReview(Task task) {
        System.out.println("codeReview is not allowed");
    }

    default void done(Task task) {
        System.out.println("done is not allowed");
    }

    default void toDo(Task task) {
        System.out.println("to do is not allowed");
    }
}
