package com.group.model;

public class User {
    private static int counter;
    private int userCounter;
    private boolean isCanceled;
    private String name;
    private Task task;

    public User(String name) {
        counter++;
        this.userCounter = counter;
        this.name = "User " + userCounter + " - " + name;
    }

    public void setTask(Task task) {
        if (this.task == null && !task.isChecked()) {
            this.task = task;
            task.inProgress();
            task.setChecked(true);
        }
    }

    public void validate() {
        if (isCanceled && task.isChangeFromProgressToToDo()) {
            this.task = null;
        }
    }
    public void secondValidate() {
        if (isCanceled && task.isChangeFromCodeReviewToDone()) {
            this.task = null;
        }
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void setCanceled(boolean canceled) {
        isCanceled = canceled;
    }

    public String getName() {
        return name;
    }

    public Task getTask() {
        return task;
    }

    @Override
    public String toString() {
        return userCounter + ": " +
                "name='" + name + '\'' +
                ", task=" + task;
    }

}
