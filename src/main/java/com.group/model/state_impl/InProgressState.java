package com.group.model.state_impl;

import com.group.model.State;
import com.group.model.Task;

public class InProgressState implements State {
    @Override
    public void toDo(Task task) {
        kanban.moveFromProgressToToDo(task);
        task.setChangeFromProgressToToDo(true);
        task.setState(new ToDoState());
    }

    public void codeReview(Task task) {
        kanban.moveFromInProgressToCodeReview(task);
        task.setState(new CodeReviewState());
    }

    @Override
    public String toString() {
        return "InProgressState";
    }
}
