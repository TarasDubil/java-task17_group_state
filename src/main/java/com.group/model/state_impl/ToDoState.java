package com.group.model.state_impl;

import com.group.model.State;
import com.group.model.Task;

public class ToDoState implements State {

    @Override
    public void newTask(String name) {
        kanban.addToToDoList(name);
        System.out.println("added new task");
    }

    @Override
    public void inProgress(Task task) {
        kanban.moveFromToDoToInProgress(task);
        task.setState(new InProgressState());
    }

    @Override
    public String toString() {
        return "ToDoState";
    }
}
