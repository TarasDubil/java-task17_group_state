package com.group.model.state_impl;

import com.group.model.State;

public class DoneState implements State {

    @Override
    public String toString() {
        return "DoneState";
    }
}
