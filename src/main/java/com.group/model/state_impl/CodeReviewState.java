package com.group.model.state_impl;

import com.group.model.State;
import com.group.model.Task;

public class CodeReviewState implements State {
    @Override
    public void done(Task task) {
        kanban.moveFromCodeReviewToDone(task);
        task.setChangeFromCodeReviewToDone(true);
        task.setState(new DoneState());
    }

    @Override
    public void inProgress(Task task) {
        kanban.moveFromCodeReviewToInProgress(task);
        task.setState(new InProgressState());
    }

    @Override
    public String toString() {
        return "CodeReviewState";
    }
}
