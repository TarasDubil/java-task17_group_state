package com.group.model;

import java.util.ArrayList;
import java.util.List;

public class Kanban {

    private static Kanban ourInstance = null;
    private List<Task> toDoList = new ArrayList<>();
    private List<Task> inProgressList = new ArrayList<>();
    private List<Task> codeReviewList = new ArrayList<>();
    private List<Task> doneList = new ArrayList<>();

    public static Kanban getInstance() {
        if (ourInstance == null) {
            ourInstance = new Kanban();
        }
        return ourInstance;
    }

    private Kanban() {
    }

    public void addToToDoList(String taskDescription) {
        toDoList.add(new Task(taskDescription));
    }

    public void moveFromToDoToInProgress(Task task) {
        inProgressList.add(task);
        toDoList.remove(task);
    }

    public void moveFromProgressToToDo(Task task) {
        toDoList.add(task);
        inProgressList.remove(task);
    }

    public void moveFromInProgressToCodeReview(Task task) {
        codeReviewList.add(task);
        inProgressList.remove(task);
    }

    public void moveFromCodeReviewToInProgress(Task task) {
        inProgressList.add(task);
        codeReviewList.remove(task);
    }

    public void moveFromCodeReviewToDone(Task task) {
        doneList.add(task);
        codeReviewList.remove(task);
    }

    public List<Task> getToDoList() {
        return toDoList;
    }

    public List<Task> getInProgressList() {
        return inProgressList;
    }

    public List<Task> getCodeReviewList() {
        return codeReviewList;
    }

    public List<Task> getDoneList() {
        return doneList;
    }

    public void showAllTasks() {
        System.out.println("ToDo list: " + toDoList);
        System.out.println("In progress list: " + inProgressList);
        System.out.println("CodeReview list: " + codeReviewList);
        System.out.println("Done list: " + doneList);
    }
}
