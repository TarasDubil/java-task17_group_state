package com.group.model;

import com.group.model.state_impl.ToDoState;

public class Task {
    private static int counter = 0;
    private boolean isChecked;
    private boolean changeFromProgressToToDo;
    private boolean changeFromCodeReviewToDone;
    private int taskCounter;
    private State state;
    private String name;

    public Task(String name) {
        counter++;
        this.taskCounter = counter;
        this.name = "Task " + taskCounter + " - " + name;
        this.state = new ToDoState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void inToDo() {
        state.toDo(this);
    }

    public void inProgress() {
        state.inProgress(this);
    }

    public void inCodeReview() {
        state.codeReview(this);
    }

    public void inDone() {
        state.done(this);
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChangeFromProgressToToDo() {
        return changeFromProgressToToDo;
    }

    public void setChangeFromProgressToToDo(boolean changeFromProgressToToDo) {
        this.changeFromProgressToToDo = changeFromProgressToToDo;
    }

    public boolean isChangeFromCodeReviewToDone() {
        return changeFromCodeReviewToDone;
    }

    public void setChangeFromCodeReviewToDone(boolean changeFromCodeReviewToDone) {
        this.changeFromCodeReviewToDone = changeFromCodeReviewToDone;
    }

    @Override
    public String toString() {
        return taskCounter + ": " +
                "state=" + state +
                ", name='" + name;
    }
}
