package com.group.controller;

import com.group.model.Task;

import java.util.List;

public interface Controller {
    void createTask(String taskDescription);

    void changeTaskStatus();

    void createUser(String name);

    void getAllTasks();

    void giveUserTask(Task task);

    List<Task> getTasks();
}
