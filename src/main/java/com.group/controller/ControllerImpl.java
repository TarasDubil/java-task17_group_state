package com.group.controller;

import com.group.model.Kanban;
import com.group.model.Task;
import com.group.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    private Kanban kanban;
    private List<User> users;
    private User user;

    public ControllerImpl() {
        kanban = Kanban.getInstance();
        users = new ArrayList<>();
    }

    public List<User> getUsers() {
        return users;
    }

    public void createTask(String taskDescription) {
        kanban.addToToDoList(taskDescription);
    }

    public void createUser(String name) {
        User user = new User(name);
        users.add(user);
    }

    public void getAllTasks() {
        kanban.showAllTasks();
    }

    public void giveUserTask(Task task) {
        user.setTask(task);
    }

    public List<Task> getTasks() {
        return kanban.getToDoList();
    }

    public void changeTaskStatus() {
        Task task = user.getTask();
        if (task != null) {
            System.out.println("1 : InToDo\n2 : inProgress\n3 : inCodeReview\n4 : inDone");
            Scanner scanner = new Scanner(System.in);
            int i = scanner.nextInt();
            switch (i) {
                case 1:
                    task.inToDo();
                    user.setCanceled(true);
                    user.validate();
                    break;
                case 2:
                    task.inProgress();
                    break;
                case 3:
                    task.inCodeReview();
                    break;
                case 4:
                    task.inDone();
                    user.setCanceled(true);
                    user.secondValidate();
                    break;
            }
        } else {
            System.out.println("User hasn't any tasks");
        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
